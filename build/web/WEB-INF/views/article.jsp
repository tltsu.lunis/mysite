<%-- 
    Document   : article
    Created on : 11.11.2021, 11:24:42
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

         <div id="main">
            <aside class="leftAside">
                <h2>Разделы</h2>
                <ul>
                    <li><a href="#">Лекарственные препараты</a></li>
                    <li><a href="#">Дезинфицирующие средства</a></li>
                    <li><a href="#">Медицинские изделия</a></li>
                    <li><a href="#">Оптика</a></li>
                    
                </ul>
            </aside>
            <section>
                <article>
                    <h1>${article.title}</h1>
                    <div class="text-article">
                        ${article.text}
                    </div>
                    <a href="aredit?id=${article.id}">Изменить</a>
                    <a href="ardelete?id=${article.id}">Удалить</a>
                    <div class="fotter-article">
                        <span class="autor">Автор статьи: <a href="#">администратор</a></span>                        
                        <span class="date-article">Дата статьи: ${article.date}</span>
                    </div>
                </article>
            </section>
        </div>


